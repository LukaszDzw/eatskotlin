package com.dzwonk.eats

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.HashMap

class HomeActivity : AppCompatActivity() {

    private lateinit var textMessage: TextView

    private var fragmentsEnumHashMap: HashMap<Int, FragmentsEnum> = HashMap()

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        item.isChecked = true

        if (fragmentsEnumHashMap.containsKey(item.itemId)) {
            val fragmentsEnum = fragmentsEnumHashMap.get(item.itemId)

            val fragment = getFragmentForTag(fragmentsEnum!!)
            replaceFragment(fragment, fragmentsEnum!!)
        }

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //todo:
        //fragmentsEnumHashMap[R.id.navigation_offers] = FragmentsEnum.OffersTag
        //fragmentsEnumHashMap[R.id.navigation_favorite_offers] = FragmentsEnum.FavoritesOffersTag

        textMessage = findViewById(R.id.message)
        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    fun getFragmentForTag(fragmentsEnum: FragmentsEnum): Fragment? {
        val fragment = supportFragmentManager.findFragmentByTag(fragmentsEnum.name)

        if (fragment != null) {
            return fragment
        }

/*        when (fragmentsEnum) {
            FragmentsEnum.OffersTag -> return OffersFragment.newInstance()
            FragmentsEnum.FavoritesOffersTag -> return FavoritesOffersFragment.newInstance("", "")
        }*/

        return fragment
    }

    fun replaceFragment(fragment: Fragment, fragmentsEnum :FragmentsEnum)
    {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.contentRoot, fragment, fragmentsEnum.name)
            .addToBackStack(fragmentsEnum.name)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }
}
