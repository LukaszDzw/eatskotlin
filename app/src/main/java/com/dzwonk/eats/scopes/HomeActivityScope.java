package com.dzwonk.eats.scopes;

import javax.inject.Scope;

@Scope
public @interface HomeActivityScope {}
